﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TPReseuaxNeurones
{
    public partial class Form1 : Form
    {
        private bool flag;
        private int[] vector;
        public Form1()
        {
            InitializeComponent();
            vector = new int[64];
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.FillRectangle(Brushes.White, 0,0, (panel1.Width), (panel1.Height));
            for (int i = 0; i < 64; i++)
            {
                if (vector[i] == 1)
                {
                    int l = i / 8;
                    int c = i % 8;
                    int y = l * (panel1.Height / 8);
                    int x = c * (panel1.Width / 8);
                    e.Graphics.FillRectangle(Brushes.Black, x, y, (panel1.Width / 8), (panel1.Height / 8));
                }
            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            flag = true;
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (flag == false)
            {
                return;
            }
            int l = e.Y / (panel1.Height / 8);
            int c = e.X / (panel1.Width / 8);
            int index = 8 * l + c;
            vector[index] = 1;
            panel1.Invalidate(true);
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            flag = false;
        }

        private void clear_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 64; i++)
            {
                vector[i] = 0;

            }
            panel1.Invalidate(true);
        }

        private void plus_Click(object sender, EventArgs e)
        {
            StreamWriter sw;
            sw = new StreamWriter(Directory.GetCurrentDirectory()+"\\base.txt", true);
            string s = ConvertVector();
            sw.WriteLine(s);
            sw.WriteLine(lettre.Text);
            sw.Close();
            
            
        }
        private string ConvertVector()
        {
            string s = "";
            for (int i = 0; i < 64; i++)
            {
                if (vector[i]==0)
                    s += "0";
                else
                    s += "1";
            }
            return s;
        }
    }
}
